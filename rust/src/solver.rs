use std::fmt;
use rand::Rng;
use crate::ant::Ant;
use crate::graph::Graph;
use crate::segment::Segment;
use rayon::prelude::*;

/// Solver for TSP problem
/// Instanciate a Graph and Ants to find best paths
pub struct Solver {
    nb_node: usize,
    pub graph: Graph,
    pub ant_colony: Vec<Ant>,
    pub nb_ants: usize,
    pub best_path_length: f32,
    pub nb_elite_ants: usize,
    pub elite_ant_colony: Vec<Ant>,
    pub elite_ant_weight: f32,
    stagnation_count: u16
}

fn walk_ant(ant: &mut Ant, graph: &Graph) {
    let mut rng = rand::thread_rng();
    let mut roll: f32;
    let segment: &Segment;
    ant.reset_path();
    // Iterate for number of segments in the path (minus last segment)
    for node_number in 0..graph.nb_node-1 {
        roll = rng.gen::<f32>();
        let (next_node, walk_distance) = graph.get_next_node(ant.current_node, &ant.available_nodes, roll);
        // Check for invalid path
        if next_node == usize::MAX {
            ant.reset_path();
            ant.path_length = f32::MAX;
            ant.path_length_floor = u32::MAX;
            break;
        }
        ant.walk_to_node(node_number + 1, next_node, walk_distance);
    }
    // Add last segment
    if ant.path_length_floor < u32::MAX {
        segment = &graph.segment_map[ant.current_node][ant.start_node];
        if segment.available {
            ant.walk_to_node(graph.nb_node, segment.end_node, segment.distance);
        } else {
            ant.reset_path();
            ant.path_length = f32::MAX;
            ant.path_length_floor = u32::MAX;
        }
    }
}

impl Solver {
    /// Create new solver
    pub fn new(nb_node: usize, distance_map: Vec<Vec<f32>>) -> Self {
        let nb_ants: usize = 2*nb_node;
        let nb_elite_ants: usize = nb_node/2;
        let mut ant_colony: Vec<Ant> = vec![];
        let mut ant: Ant;
        for ant_number in 0..nb_ants {
            ant = Ant::new(nb_node);
            ant.init(ant_number % nb_node);
            ant_colony.push(ant)
        }
        let mut elite_ant_colony: Vec<Ant> = vec![];
        for _ in 0..nb_elite_ants {
            elite_ant_colony.push(Ant::new(nb_node))
        }
        Self {
            nb_node: nb_node,
            nb_ants: nb_ants,
            graph: Graph::new(nb_node, distance_map),
            ant_colony: ant_colony,
            best_path_length: f32::MAX,
            nb_elite_ants:nb_elite_ants,
            elite_ant_colony: elite_ant_colony,
            elite_ant_weight: 1.0,
            stagnation_count: 0
        }
    }

    /// Setup graph pheromons and graph constants
    pub fn init(&mut self) {
        self.graph.reset_pheromons();
        self.graph.compute_inverted_distance();
    }

    /// Reset ants and make them walk the graph again.
    pub fn walk_ants(&mut self) {

        self.ant_colony
        .par_iter_mut()
        .for_each(|ant| walk_ant(ant, &self.graph));
        // self.walk_ant(ant_number);
    }

    /// Update elite ant colony with ants from current basic colony
    pub fn update_elite_ant_colony(&mut self) {
        let mut elite_colony_update: bool = false;
        self.ant_colony.sort_by(|b, a| b.path_length_floor.cmp(&a.path_length_floor));
        // Sort ants by shortest path
        for ant_number in 0..self.elite_ant_colony.len() {
            for elite_ant_number in ant_number..self.elite_ant_colony.len() {
                let ant = &self.ant_colony[ant_number];
                let elite_ant = &mut self.elite_ant_colony[elite_ant_number];
                if ant.path_complete && ant.path_length_floor < elite_ant.path_length_floor {
                    elite_colony_update = true;
                    elite_ant.path_length = ant.path_length;
                    elite_ant.path_length_floor = ant.path_length_floor;
                    elite_ant.path_complete = ant.path_complete;
                    for node in 0..ant.path.len() {
                        elite_ant.path[node] = ant.path[node];
                    }
                    break;
                }
            }
        }
        self.best_path_length = self.elite_ant_colony[0].path_length;
        if elite_colony_update {
            self.stagnation_count = 0;
        } else {
            self.stagnation_count = self.stagnation_count + 1;
        }
    }

    /// Update pheromons based on an ant colony
    fn update_colony_pheromons(&mut self, colony_select: usize) {
        // Select colony
        let colony: &Vec<Ant>;
        let ant_weight: f32;
        match colony_select {
            0 => {
                colony = &self.ant_colony;
                ant_weight = 1.0;
            }
            _ => {
                colony = &self.elite_ant_colony;
                ant_weight = self.elite_ant_weight;
            }
        }

        // Iterate over ants
        for ant_number in 0..colony.len() {
            let ant = &colony[ant_number];
            // Only update pheromons for valid paths
            if ant.path_complete && ant.path_length > 0.0 {
                let pheromon: f32 = ant_weight/ant.path_length;
                for node in 0..ant.path.len()-1 {
                    self.graph.segment_map[ant.path[node]][ant.path[node+1]].add_pheromon_weight(pheromon);
                    self.graph.segment_map[ant.path[node+1]][ant.path[node]].add_pheromon_weight(pheromon);
                }
            }
        }
        // Update pheromon max
        for start_node in 0..self.nb_node {
            for end_node in start_node..self.nb_node {
                let segment = &self.graph.segment_map[start_node][end_node];
                if segment.pheromon > self.graph.pheromon_max {
                    self.graph.pheromon_max = segment.pheromon;
                }
            }
        }
    }

    /// Compute new pheromons weights for the graph
    pub fn update_graph_pheromons(&mut self) {
        self.graph.evaporate_pheromons();
        self.update_colony_pheromons(0);
        self.update_colony_pheromons(1);
    }

    /// Iterate ant run to find best path
    pub fn run(&mut self) {
        self.graph.compute_weighted_coef();
        self.walk_ants();
        self.update_elite_ant_colony();
        self.update_graph_pheromons();
        println!("{} {} {}", self.stagnation_count, self.best_path_length, self.elite_ant_weight);
        if self.stagnation_count >= 20 {
            self.stagnation_count = 0;
            self.elite_ant_weight = rand::thread_rng().gen_range(0.0..3.0);
            self.graph.reset_pheromons();
        }
    }
}

impl fmt::Debug for Solver {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Solver")
         .field("nb_node", &self.nb_node)
         .field("nb_ants", &self.nb_ants)
         .field("ant_colony", &self.ant_colony)
         .field("best_path_length", &self.best_path_length)
         .field("nb_elite_ants", &self.nb_elite_ants)
         .field("elite_ant_colony", &self.elite_ant_colony)
         .field("stagnation_count", &self.stagnation_count)
         .finish()
    }
}