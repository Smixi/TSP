mod segment;
mod graph;
mod ant;
mod solver;

use solver::Solver;
use std::env;
use std::fs;
use regex::Regex;

use piston_window::*;

/// Import distance map and node list from formatted file
fn get_graph_definition(filename: &String) -> (Vec<Vec<f32>>, usize, Vec<(f64, f64)>) {

    let file_dump = fs::read_to_string(filename)
    .expect("Something went wrong reading the file");

    let mut nb_node: usize = 0;
    let re = Regex::new(r"(\d+)\s+\d+\s*\r?\n").unwrap();
    for cap in re.captures_iter(&file_dump) {
        nb_node = cap[1].parse::<usize>().unwrap();
    }

    let mut node_map: Vec<(f64, f64)> = vec![(0.0, 0.0); nb_node];
    let mut node: usize;
    let mut x_pos: f64;
    let mut y_pos: f64;
    let re = Regex::new(r"(\d+)\s+([\.\d]+)\s+([\.\d]+)\s+\w+\r?\n").unwrap();
    for cap in re.captures_iter(&file_dump) {
        node = cap[1].parse::<usize>().unwrap();
        x_pos = cap[2].parse::<f64>().unwrap();
        y_pos = cap[3].parse::<f64>().unwrap();
        node_map[node] = (x_pos, y_pos);
    }

    let mut distance_map: Vec<Vec<f32>> = vec![vec![0.0; nb_node]; nb_node];
    let mut start_node: usize;
    let mut end_node: usize;
    let mut distance: f32;
    let re = Regex::new(r"(\d+)\s+(\d+)\s+([\.\d]+)\s*\r?\n").unwrap();
    for cap in re.captures_iter(&file_dump) {
        start_node = cap[1].parse::<usize>().unwrap();
        end_node = cap[2].parse::<usize>().unwrap();
        distance = cap[3].parse::<f32>().unwrap();
        distance_map[start_node][end_node] = distance;
        distance_map[end_node][start_node] = distance;
    }

    return (distance_map, nb_node, node_map);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename: &String = &args[1];

    let (distance_map, nb_node, node_map) = get_graph_definition(filename);

    let mut solver: Solver = Solver::new(nb_node, distance_map);
    solver.init();

    solver.run();

    let mut window: PistonWindow =
    WindowSettings::new("Hello World!", [1024, 512])
        .build().unwrap();
    while let Some(e) = window.next() {

        window.draw_2d(&e, |c, g, _| {
            solver.run();
            clear([0.0, 0.0, 512.0, 512.0], g);
            let path = &solver.elite_ant_colony[0].path;
            for node in 0..path.len()-1 {
                let start_node = path[node];
                let end_node = path[node+1];
                let start_pos = node_map[start_node];
                let end_pos = node_map[end_node];
                let x_start = 256.0*start_pos.0 + 128.0;
                let y_start = 256.0 - 256.0*start_pos.1 + 128.0;
                let x_end = 256.0*end_pos.0 + 128.0;
                let y_end = 256.0 - 256.0*end_pos.1 + 128.0;
                line_from_to([1.0, 1.0, 0.0, 1.0],
                    1.0,
                    [x_start, y_start],
                    [x_end, y_end],
                    c.transform, g);
            }
            for start_node in 0..nb_node {
                for end_node in start_node+1..nb_node {
                    let segment = &solver.graph.segment_map[start_node][end_node];
                    let start_pos = node_map[start_node];
                    let end_pos = node_map[end_node];
                    let x_start = 256.0*start_pos.0 + 640.0;
                    let y_start = 256.0 - 256.0*start_pos.1 + 128.0;
                    let x_end = 256.0*end_pos.0 + 640.0;
                    let y_end = 256.0 - 256.0*end_pos.1 + 128.0;
                    let alpha = segment.pheromon/solver.graph.pheromon_max;
                    line_from_to([1.0, 1.0, 0.0, alpha],
                        1.0,
                        [x_start, y_start],
                        [x_end, y_end],
                        c.transform, g);
                }
            }
        });
    }
}
