use std::fmt;

/// Representation of a graph segment.
/// Holds the start and end nodes, and weighted distance/pheromon
#[derive(Copy, Clone)]
pub struct Segment {
    pub start_node: usize,
    pub end_node: usize,
    pub available: bool,
    pub distance:f32,
    inverted:f32,
    pub pheromon:f32,
    pub weighted:f32
}

impl Segment {
    /// Create empty segment
    pub fn new() -> Self {
        Self {
            start_node: 0,
            end_node: 0,
            available: false,
            distance: 0.0,
            inverted: 0.0,
            pheromon: 0.0,
            weighted: 0.0
        }
    }

    /// Init segment with nodes and distance
    pub fn init(&mut self, start_node: usize, end_node: usize, distance: f32) {
        self.start_node = start_node;
        self.end_node = end_node;
        self.distance = distance;
        self.available = self.distance > 0.0;
    }

    /// Set pheromon level
    pub fn reset_pheromon(&mut self, pheromon: f32) {
        self.pheromon = pheromon;
    }

    /// Compute weighted inverted distance
    pub fn compute_inverted_distance(&mut self, beta: f32) {
        if self.distance > 0.0 {
            self.inverted = f32::powf(1.0/self.distance, beta);
        } else {
            self.inverted = 0.0;
        }
    }

    /// Increase pheromon level with input amount
    pub fn add_pheromon_weight(&mut self, pheromon: f32) {
        self.pheromon = self.pheromon + pheromon;
    }

    /// Reduce pheromon level by input ratio
    pub fn evaporate_pheromon(&mut self, rho: f32) {
        self.pheromon = self.pheromon * rho;
    }

    /// Compute total segment weight
    pub fn compute_weighted_coef(&mut self, alpha: f32) {
        self.weighted = self.inverted * f32::powf(self.pheromon, alpha);
    }
}

impl fmt::Debug for Segment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Segment")
         .field("start_node", &self.start_node)
         .field("end_node", &self.end_node)
         .field("available", &self.available)
         .field("distance", &self.distance)
         .field("inverted", &self.inverted)
         .field("pheromon", &self.pheromon)
         .field("weighted", &self.weighted)
         .finish()
    }
}