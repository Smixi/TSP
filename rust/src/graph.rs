use std::fmt;
use crate::segment::Segment;

/// Holds the matrix of segments which constitutes the graph
pub struct Graph {
    pub nb_node: usize,
    pub alpha: f32,
    pub beta: f32,
    pub rho: f32,
    pub epsilon:f32,
    pub segment_map: Vec<Vec<Segment>>,
    pub pheromon_max: f32
}

impl Graph {
    /// Get graph instance with distance map converted to segment list representation
    pub fn new(nb_node: usize, distance_map: Vec<Vec<f32>>) -> Self {
        // Populate segment map with information from distance map
        let mut segment_map:Vec<Vec<Segment>> = vec![vec![Segment::new(); nb_node]; nb_node];
        for start_node in 0..nb_node {
            for end_node in start_node..nb_node {
                segment_map[start_node][end_node].init(start_node, end_node, distance_map[start_node][end_node]);
                segment_map[end_node][start_node].init(end_node, start_node, distance_map[start_node][end_node]);
            }
        }
        // Create self
        Self {
            nb_node: nb_node,
            alpha: 1.0,
            beta: 2.0,
            rho: 0.5,
            epsilon:0.00001,
            segment_map: segment_map,
            pheromon_max: 0.00001
        }
    }

    /// Reset pheromon weigths
    pub fn reset_pheromons(&mut self) {
        self.pheromon_max = self.epsilon;
        for start_node in 0..self.nb_node {
            for end_node in 0..self.nb_node {
                self.segment_map[start_node][end_node].reset_pheromon(self.epsilon);
            }
        }
    }

    /// Inverse distance and raise to power beta for each segment
    /// Distances equal to 0 are considered unavailable paths.
    pub fn compute_inverted_distance(&mut self) {
        for start_node in 0..self.nb_node {
            for end_node in 0..self.nb_node {
                self.segment_map[start_node][end_node].compute_inverted_distance(self.beta);
            }
        }
    }

    /// Reduce quantity of pheromons across the whole graph
    pub fn evaporate_pheromons(&mut self) {
        for start_node in 0..self.nb_node {
            for end_node in 0..self.nb_node {
                self.segment_map[start_node][end_node].evaporate_pheromon(self.rho);
            }
        }
    }

    /// Compute weight for each segment
    pub fn compute_weighted_coef(&mut self) {
        for start_node in 0..self.nb_node {
            for end_node in 0..self.nb_node {
                self.segment_map[start_node][end_node].compute_weighted_coef(self.alpha);
            }
        }
    }

    /// Randomize next node on path using weighted map available node list
    pub fn get_next_node(&self, start_node: usize, available_nodes:&Vec<bool>, roll: f32) -> (usize, f32) {
        // Compute sum of weights and roll a number in this range
        let mut weight_sum: f32 = 0.0;
        for candidate_node in 0..self.nb_node {
            if available_nodes[candidate_node] {
                weight_sum = weight_sum + self.segment_map[start_node][candidate_node].weighted;
            }
        }
        let mut roll: f32 = roll * weight_sum;
        // Special exit case for paths impossible to complete
        if weight_sum <= 0.0 {
            return (usize::MAX, 0.0);
        }
        // Iterate candidate nodes until segment weight is higher than rolled weight
        for candidate_node in 0..self.nb_node {
            let segment = &self.segment_map[start_node][candidate_node];
            if available_nodes[candidate_node] && segment.available && segment.weighted >= roll {
                return (candidate_node, segment.distance);
            } else if available_nodes[candidate_node] && segment.available {
                roll = roll - segment.weighted;
            }
        }
        // Should not reach here
        return (usize::MAX, 0.0);
    }
}

impl fmt::Debug for Graph {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Graph")
         .field("nb_node", &self.nb_node)
         .field("segment_map", &self.segment_map)
         .finish()
    }
}