use std::fmt;

/// Representation of an ant.
/// Will remember its path through the graph
pub struct Ant {
    nb_node: usize,
    pub start_node: usize,
    pub current_node: usize,
    pub path_length: f32,
    pub path_length_floor: u32,
    pub path: Vec<usize>,
    pub available_nodes: Vec<bool>,
    pub path_complete: bool,
}

impl Ant {
    /// Create ant
    pub fn new(nb_node: usize) -> Self {
        Self {
            nb_node: nb_node,
            start_node: 0,
            current_node: 0,
            path_length: f32::MAX,
            path_length_floor: u32::MAX,
            path: vec![0; nb_node + 1],
            available_nodes: vec![true; nb_node],
            path_complete: false
        }
    }

    /// Init ant with starting node
    pub fn init(&mut self, start_node: usize) {
        self.start_node = start_node;
        self.path[0] = start_node;
    }

    /// Rest path contents and available nodes
    pub fn reset_path(&mut self) {
        self.current_node = self.start_node;
        for node in 0..self.nb_node {
            self.available_nodes[node] = true;
        }
        self.available_nodes[self.start_node] = false;
        self.path_complete = false;
        self.path_length = 0.0;
        self.path_length_floor = 0;
    }

    /// Set current node and update path contents
    pub fn walk_to_node(&mut self, node_number: usize, next_node: usize, walk_distance: f32) {
        self.path[node_number] = next_node;
        self.available_nodes[next_node] = false;
        self.path_length = self.path_length + walk_distance;
        self.path_length_floor = self.path_length as u32;
        self.current_node = next_node;
        self.path_complete = self.current_node == self.start_node;
    }
}

impl fmt::Debug for Ant {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Ant")
         .field("nb_node", &self.nb_node)
         .field("start_node", &self.start_node)
         .field("current_node", &self.current_node)
         .field("path_length", &self.path_length)
         .field("path_length_floor", &self.path_length_floor)
         .field("path_complete", &self.path_complete)
         .field("available_nodes", &self.available_nodes)
         .field("path", &self.path)
         .finish()
    }
}