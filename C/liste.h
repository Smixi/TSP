#ifndef _LISTE
#define _LISTE

typedef struct ARC{
    unsigned int dep;
    unsigned int arr;
    double dist;
    double pher;
} ARC;

struct cellule1 {
    ARC val;
    struct cellule1 *suiv;
    };

typedef struct cellule1* Liste;

void affiche(ARC* val);
Liste creer_liste();
int liste_vide(Liste L);
void visualiser_liste(Liste L);
Liste ajout_tete(ARC val, Liste L);
Liste ajout_queue(ARC val, Liste L);
ARC lecture_n(Liste L, int n);
ARC* rechercher_liste(Liste L, int dep, int arr);
#endif
