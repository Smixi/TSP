#ifndef _FILE
#define _FILE
#include <stdio.h>
#include "liste.h"


struct cellule {
    ARC* val;
    struct cellule *suiv;
    };

typedef struct cellule* File;

File creer_file();
int file_vide(File f);
void visualiser_file(File f);
File enfiler(ARC* val, File f);
int appartient_file(File f, ARC* val);
#endif
