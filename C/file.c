#include <stdio.h>
#include <stdlib.h>
#include "structure.h"
#include "file.h"

File creer_file(){
    return NULL;
}

int file_vide(File f){
    return !f;
}

void visualiser_file(File f){
    if(!file_vide(f)){
    File p;
    for(p=f; p->suiv!=f; p=p->suiv){
        affiche(p->val);
    }
    affiche(p->val);
    }
}

File enfiler(ARC* val, File f){
    File p=calloc(1, sizeof(*p));
    if (p==NULL) {perror("Erreur allocation fonction enfiler\n"); return NULL;}
    if(file_vide(f)){
        p->val=val;
        p->suiv=p;
        return p;
    }
    p->val=val;
    p->suiv=f->suiv;
    f->suiv=p;
    return p;
}

int appartient_file(File f, ARC* val){
    if(file_vide(f)) return 0;
    File q=NULL;
    if(f->val->dep == val->dep && f->val->arr == val->arr) return 1;
    for(q=f->suiv; q!=f; q=q->suiv){
        if(q->val->dep == val->dep && q->val->arr == val->arr) return 1;
    }
    return 0;
}

