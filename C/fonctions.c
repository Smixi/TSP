#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "structure.h"
#include "liste.h"
#include "fonctions.h"
#include "file.h"
#include <SDL_phelma.h>
#include "affichage.h"
#include <time.h>

double pher_max=0;

void visualiser_table(SOMMET* table, int nb_sommet){
    int i;
    for(i=0; i<nb_sommet; i++){
        printf("\n%s : \n", table[i].nom);
        visualiser_liste(table[i].voisins);
    }
}

SOMMET* creation_graphe(char* fichier, int* nb_sommet, int* nb_arc){
    char ligne[512];
    SOMMET* table=NULL;
    int i;
    FILE* f1=NULL;
    f1 = fopen(fichier, "r");
    if (f1 == NULL) {perror( "Erreur ouverture lecture fichier texte\n" ); exit(1);}
    fgets(ligne, sizeof(ligne), f1);
    sscanf(ligne, "%d %d", nb_sommet, nb_arc);
    fgets(ligne, sizeof(ligne), f1);

    table = calloc(*nb_sommet, sizeof(*table));
    if (table == NULL){ perror( "Erreur création table\n" ); exit(1);}

    for(i=0; i<*nb_sommet; i++){
        fgets(ligne, sizeof(ligne), f1);
        sscanf(ligne, "%d %lf %lf %s", &table[i].numero, &table[i].x, &table[i].y, table[i].nom);
    }

    fgets(ligne, sizeof(ligne), f1);

    int sommet1, sommet2;
    double distance;
    for(i=0; i<*nb_arc; i++){
        fgets(ligne, sizeof(ligne), f1);
        sscanf(ligne, "%d %d %lf", &sommet1, &sommet2, &distance);
        ARC val;
        val.dep=sommet1;
        val.arr=sommet2;
        val.dist=distance;
        val.pher=EPSILON;
        table[sommet1].voisins = ajout_queue(val, table[sommet1].voisins);
        val.dep=sommet2;
        val.arr=sommet1;
        table[sommet2].voisins = ajout_queue(val, table[sommet2].voisins);
    }
    fclose(f1);
    return table;
}

int appartient_tab(unsigned int* tabu, int taille, int sommet){
    if(taille == 0) return 0;
    int i;
    for(i=0; i<taille; i++){
        if(tabu[i]==sommet) return 1;
    }
    return 0;
}

FOURMIS* initialiser_fourmis(FOURMIS* tab_fourmis, int nb_fourmis, int nb_sommet){
    int i;
    for(i=0; i<nb_fourmis; i++){
        tab_fourmis[i].ville_cour=rand()%nb_sommet;
        tab_fourmis[i].ville_dep=tab_fourmis[i].ville_cour;
        tab_fourmis[i].longueur_chemin=0;
        tab_fourmis[i].solution=NULL;
    }
    return tab_fourmis;
}


unsigned int* initialiser_tabuk(unsigned int* tabuk, int nb_sommet, FOURMIS* tab_fourmis, int k){
    int i;
    for(i=0; i<nb_sommet; i++) tabuk[i]=0;
    tabuk[tab_fourmis[k].ville_dep]=1;
    return tabuk;
}


void evaporer_pher(SOMMET* table, int nb_sommet){
    int i;
    for(i=0; i<nb_sommet; i++){
        Liste p=table[i].voisins;
        while(!liste_vide(p)){
            p->val.pher=p->val.pher*RHO;
            p=p->suiv;
        }
    }
}

void reset_pher(SOMMET* table, int nb_sommet){
    int i;
    for(i=0; i<nb_sommet; i++){
        Liste p=table[i].voisins;
        while(!liste_vide(p)){
            p->val.pher=EPSILON;
            p=p->suiv;
        }
    }
}

double actualiser_pher(double longueur, File chemin){
    int i;
    double taux_pher= Q/longueur;
    File p=chemin;
    if(file_vide(p)) return pher_max;
    p->val->pher=p->val->pher + taux_pher;
    if(p->val->pher>pher_max) pher_max =p->val->pher;
    for(p=chemin->suiv;p!=chemin;p=p->suiv){
        p->val->pher=p->val->pher + taux_pher;
        if(p->val->pher>pher_max) pher_max =p->val->pher;
    }
    return pher_max;
}

File* actualiser_tab_elite(double* tab_longueur_elite, File* tab_elite, File chemin_elite, double longueur_elite, int* stagne){
    int i=0;
    int k;
    File p;
    while(i<NB_ELITE && longueur_elite<tab_longueur_elite[i]){
        i++;
    }
    if(i<NB_ELITE && longueur_elite==tab_longueur_elite[i]){
        tab_elite;
    }
    i--;
    for(k=0; k<i; k++){
        tab_elite[k]=tab_elite[k+1];
        tab_longueur_elite[k]=tab_longueur_elite[k+1];
    }
    tab_elite[i]=chemin_elite;
    tab_longueur_elite[i]=longueur_elite;
    if(STAGNE)*stagne=0;
    return tab_elite;
}


ARC* choix_ville(unsigned int* tabuk, Liste voisins){
    double tirage = rand()/ (double) RAND_MAX;
    double cumul_proba = 0;
    Liste j=NULL;
    for(j=voisins; !liste_vide(j); j=j->suiv){
        if(tabuk[j->val.arr]==0){
            cumul_proba = cumul_proba + pow(j->val.pher,alpha) * pow(1./j->val.dist, beta);
        }
    }
    double fx = 0;
    for(j=voisins; !liste_vide(j); j=j->suiv){
        if(tabuk[j->val.arr]==0){
            fx = fx + (pow(j->val.pher,alpha) * pow(1./j->val.dist, beta)) / cumul_proba;
            if(tirage <= fx) return &(j->val);
        }
    }
    return NULL;
}



double TSP(char* fichier, int affichage, double* temps, File *chemin_opti){
    time_t t;
    srand((unsigned) time(&t));
    SDL_Surface*  s1=NULL;
    int nb_sommet;
    int nb_arc;
    int i; //nombre d'itérations réalisées
    int k; //indice fourmi
    int stagne=0;
    int taille_tabuk; //nombre de sommets visités par la fourmi k
    ARC* p_arc=NULL;
    unsigned int* tabuk=NULL;
    FOURMIS* tab_fourmis = NULL;
    SOMMET* table=NULL; //la variable table correspond au graphe
    table = creation_graphe(fichier, &nb_sommet, &nb_arc);
    //visualiser_table(table, nb_sommet);
    int nb_fourmis = COEF_FOURMIS*nb_sommet;
    double dimx = 0; //variable d'affichage, dimx = plus grande des coordonnées en x
    double dimy = 0; //idem
    dim_fenetre(table, nb_sommet, &dimx, &dimy);
    if(affichage) s1 = newfenetregraphique(1000*dimx + 3*DECALAGE, 500*dimy + 2*DECALAGE);
    double* tab_longueur_elite=NULL;
    File* tab_elite=NULL;
    int nb_reset=0;
    File chemin_elite=NULL;
    double longueur_elite;
    double temps_debut;
    double temps_tot=0;
    int nb_surete=0;

    // Allocation des tableaux nécessaires.

    tab_fourmis = calloc(nb_fourmis, sizeof(*tab_fourmis));
    if (tab_fourmis == NULL){ perror( "Erreur création table fourmis\n" ); exit(1);}

    tab_longueur_elite = calloc(NB_ELITE, sizeof(*tab_longueur_elite));
    if (tab_longueur_elite == NULL){ perror( "Erreur création tab_longueur_elite\n" ); exit(1);}
    for(i=0; i<NB_ELITE; i++) tab_longueur_elite[i]=INFINITY; //permet la mise à jour des meilleures solutions

    tab_elite = calloc(NB_ELITE, sizeof(*tab_elite));
    if (tab_elite == NULL){ perror( "Erreur création table_elite\n" ); exit(1);}

    tabuk = calloc(nb_sommet, sizeof(*tabuk));
    if (tabuk == NULL){ perror( "Erreur création tabuk\n" ); exit(1);}

    //PROGRAMME PRINCIPAL
    i=0;
    temps_debut=clock();
    do{
        i++;
        tab_fourmis = initialiser_fourmis(tab_fourmis, nb_fourmis, nb_sommet);
        for(k=0; k<nb_fourmis; k++){
            tabuk = initialiser_tabuk(tabuk, nb_sommet, tab_fourmis, k);
            taille_tabuk=1;
            while(p_arc = choix_ville(tabuk, table[tab_fourmis[k].ville_cour].voisins)){
                tabuk[p_arc->arr] = 1;
                tab_fourmis[k].ville_cour=p_arc->arr;
                tab_fourmis[k].solution = enfiler(p_arc, tab_fourmis[k].solution);
                tab_fourmis[k].longueur_chemin = tab_fourmis[k].longueur_chemin  + p_arc->dist;
                taille_tabuk+=1;
            }
            if(taille_tabuk==nb_sommet){ //taille_tabuk==nb_sommet signifie que la fourmi a visité chaque sommet, reste à boucler le chemin.
                p_arc = rechercher_liste(table[tab_fourmis[k].ville_cour].voisins, tab_fourmis[k].ville_cour, tab_fourmis[k].ville_dep);
                if(p_arc!=NULL){
                    tab_fourmis[k].solution = enfiler(p_arc, tab_fourmis[k].solution);
                    tab_fourmis[k].longueur_chemin = tab_fourmis[k].longueur_chemin  + p_arc->dist;
                }
                else taille_tabuk++;
            }
            if(taille_tabuk==nb_sommet && tab_fourmis[k].longueur_chemin  < tab_longueur_elite[0]){
                chemin_elite = tab_fourmis[k].solution;
                longueur_elite = tab_fourmis[k].longueur_chemin;
                tab_elite=actualiser_tab_elite(tab_longueur_elite, tab_elite, chemin_elite, longueur_elite, &stagne);
            }
            if(taille_tabuk!=nb_sommet){
                tab_fourmis[k].solution=NULL;
            }
        }

        evaporer_pher(table, nb_sommet);
        pher_max = 0;
        if(POIDS_ELITE!=0){
			for(k=0; k<NB_ELITE; k++){
				pher_max = actualiser_pher(tab_longueur_elite[k]/POIDS_ELITE, tab_elite[k]);
			}
		}
		
        for(k=0; k<nb_fourmis; k++){
            pher_max = actualiser_pher(tab_fourmis[k].longueur_chemin, tab_fourmis[k].solution);
        }
        stagne++;
        if(stagne==MAX_CYCLE) {
            if(nb_reset != NB_RESET) reset_pher(table, nb_sommet);
            nb_reset++;
            stagne=0;
            //printf("fin itération après %d cycles (temps : %lf s) (n°%d)\n", i, (clock()-temps_debut)/CLOCKS_PER_SEC, nb_reset);
            i=0;
            temps_tot=temps_tot + (clock()-temps_debut)/CLOCKS_PER_SEC;
            temps_debut=clock();
            }

        if(affichage){
            tracer_graphe(s1, nb_sommet, table, dimy, pher_max, DECALAGE);
            tracer_sol(s1, tab_elite[NB_ELITE-1], dimx, dimy, table, DECALAGE);
        //visualiser_table(table, nb_sommet);
        }
    } while(nb_reset != NB_RESET+1); //1 premier passage + NB_RESET supplémentaires

    if(affichage) tracer_sol(s1, tab_elite[NB_ELITE-1], dimx, dimy, table, DECALAGE);
    //visualiser_file(tab_elite[NB_ELITE-1]);
    //printf("\n");
    //visualiser_table(table, nb_sommet);
    //printf("\nlongueur : %lf, temps d'exécution : %lf s\n", tab_longueur_elite[NB_ELITE-1], temps_tot);
    *temps=temps_tot;
    *chemin_opti = tab_elite[NB_ELITE-1];
    if (affichage && !file_vide(*chemin_opti)) getchar();
    if (file_vide(*chemin_opti)) printf("Aucune solution trouvée\n");
    return tab_longueur_elite[NB_ELITE-1];
}

