#include "fonctions.h"
#include <time.h>


#include <stdlib.h>
// banc_test(nomdufichier,affichage)

main(int argc, char*argv[]){
    if (argc != 3) {perror("Erreur nb d'arguments (nom du fichier, affichage(1/0))\n"); exit(1);}
    FILE* fichier_ecr = NULL;
    char nom_fichier[512]="./solution/solution_de_";

    char fichier[512];
    int iteration;
    int affichage;

    sscanf(argv[1], "%s", fichier);
    sscanf(argv[2], "%d", &affichage);


    strcat(nom_fichier, fichier);
    time_t temps;
    temps = time(NULL)- 1495100000; // :-)
    int heure = temps;              // :-)
    char date[512];
    sprintf(date, "%d", heure);
    strcat(nom_fichier, date);
    strcat(nom_fichier, ".txt");
    fichier_ecr = fopen(nom_fichier,"w+");
    if(fichier_ecr == NULL){perror("Erreur création fichier_ecr"); exit(1);}


    double longueur;
    double tempsex;
    File chemin_opti= NULL;
    longueur = TSP(fichier, affichage, &tempsex, &chemin_opti);
    File p=chemin_opti;
    if(p==NULL) fprintf(fichier_ecr, "Pas de solution\n");
    else {
        fprintf(fichier_ecr, "dep : %d, arr : %d, dist : %lf\n", p->val->dep, p->val->arr, p->val->dist);
        for(p=chemin_opti->suiv; p!=chemin_opti; p=p->suiv){
            fprintf(fichier_ecr, "dep : %d, arr : %d, dist : %lf\n", p->val->dep, p->val->arr, p->val->dist);
        }

    fprintf(fichier_ecr,"\nLongueur totale : %lf, temps d'exécution : %lf s",longueur, tempsex);
    }
    fclose(fichier_ecr);
}


