#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "affichage.h"
#include "structure.h"
#include <SDL_phelma.h>

#define EPAISSEUR 255

double max(double a, double b){
    if(a<b) return b;
    return a;
}

void Clear(SDL_Surface* s1) {
  SDL_FillRect(s1, NULL, SDL_MapRGB(s1->format,255,255,255));
}

void tracer_segment(SDL_Surface* s1, double x1, double y1, double x2, double y2, double dimy, double couleur, int decalage){
    Draw_Line(s1, x1*500 + decalage, dimy*500 + decalage - y1*500 , x2*500 + decalage, dimy*500 + decalage - y2*500, couleur);
}


void tracer_graphe(SDL_Surface* s1, int nb_sommet, SOMMET* table, double dimy, double pher_max, int decalage){
    int i;
    double phero;
    ARC* c;
    Clear(s1);
    for(i=0; i<nb_sommet; i++){
        Liste L=NULL;
        for(L=table[i].voisins; !liste_vide(L); L=L->suiv){
            if(L->val.dep<L->val.arr){
                c = rechercher_liste(table[L->val.arr].voisins, L->val.arr, L->val.dep);
                phero=max(c->pher,L->val.pher)/pher_max;
                if(phero < 0.05);
                //if (0.05 <= phero) tracer_segment(s1, table[L->val.dep].x, table[L->val.dep].y, table[L->val.arr].x, table[L->val.arr].y, dimy, 0x696969, decalage);
                if (0.05 <= phero) tracer_segment(s1, table[L->val.dep].x, table[L->val.dep].y, table[L->val.arr].x, table[L->val.arr].y, dimy, 0xF0C300, decalage);
                if (0.4 <= phero) tracer_segment(s1, table[L->val.dep].x, table[L->val.dep].y, table[L->val.arr].x, table[L->val.arr].y, dimy, 0xFFA500, decalage);
                if (0.7 <= phero) tracer_segment(s1, table[L->val.dep].x, table[L->val.dep].y, table[L->val.arr].x, table[L->val.arr].y, dimy, 0x85C17E, decalage);
            }
        }
    }
    SDL_Flip(s1);
}

void dim_fenetre(SOMMET* table, int nb_sommet, double* dimx, double* dimy){
    int i;
    for(i=0; i<nb_sommet; i++){
        if(table[i].x > *dimx) *dimx = table[i].x;
        if(table[i].y > *dimy) *dimy = table[i].y;
    }
}

void tracer_sol(SDL_Surface* s1, File solution, double dimx, double dimy, SOMMET* table, int decalage){
    if(file_vide(solution))return;
    File q=NULL;
    Draw_Line(s1, 500*table[solution->val->dep].x + 2*decalage + 500*dimx, 500*dimy + decalage - 500*(table[solution->val->dep].y), 500*(table[solution->val->arr].x) + 2*decalage + 500*dimx, 500*dimy + decalage - 500*(table[solution->val->arr].y), SDL_MapRGB(s1->format,0,66,0));
    for(q=solution->suiv; q!=solution; q=q->suiv){
        Draw_Line(s1, table[q->val->dep].x*500 + 2*decalage + 500*dimx ,500*dimy + decalage - 500*(table[q->val->dep].y), (table[q->val->arr].x)*500 + 2*decalage + 500*dimx, 500*dimy +decalage- 500*(table[q->val->arr].y), SDL_MapRGB(s1->format,0,66,0));
    }
    SDL_Flip(s1);
}

