#ifndef _STRUCTURE
#define _STRUCTURE
#include "liste.h"
#include "file.h"


typedef struct SOMMET{
    unsigned int numero;
    Liste voisins;
    double x;
    double y;
    char nom[512];
} SOMMET;


typedef struct FOURMIS{
    File solution;
    double longueur_chemin;
    unsigned int ville_dep;
    unsigned int ville_cour;
} FOURMIS;

#endif
