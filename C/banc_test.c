#include "fonctions.h"
#include <time.h>


#include <stdlib.h>
// banc_test(nomdufichier,nbdetest)

main(int argc, char*argv[]){
    if (argc != 3) {perror("Erreur nb d'arguments (nom du fichier, nombre de tests\n"); exit(1);}
    FILE* fichier_ecr = NULL;
    char nom_fichier[512]="./resultat/resultat_banc_test_";

    char fichier[512];
    int iteration;
    int affichage;

    sscanf(argv[1], "%s", fichier);
    sscanf(argv[2], "%d", &iteration);


    strcat(nom_fichier, fichier);
    time_t temps;
    temps = time(NULL)- 1495100000; // :-)
    int heure = temps;
    char date[20];
    sprintf(date, "%d", heure);
    strcat(nom_fichier, date);
    strcat(nom_fichier, ".txt");
    fichier_ecr = fopen(nom_fichier,"w+");
    if(fichier_ecr == NULL){perror("Erreur création fichier_ecr"); exit(1);}



    fprintf(fichier_ecr,"itération longueur temps MAXCYCLE:%d, RESET:%d, NBELITE:%d\n",MAX_CYCLE,NB_RESET,NB_ELITE);
    int k;
    for(k=0; k<iteration; k++){
    double longueur;
    double tempsex;
    File chemin_opti= NULL;
    longueur = TSP(fichier, 0, &tempsex, &chemin_opti);
    if(chemin_opti==NULL) fprintf(fichier_ecr, "Pas de solution\n");
    else {
        fprintf(fichier_ecr, "%d %lf %lf\n", k, longueur, tempsex);
        printf("Essai n°: %d\n",k);
        }
    }
    fclose(fichier_ecr);
}

