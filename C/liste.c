#include "liste.h"
#include <stdlib.h>
#include <stdio.h>

void affiche(ARC* val){
    printf("dep : %d, arr : %d, dist : %lf, (pher : %lf)\n", val->dep, val->arr, val->dist, val->pher);
}

Liste creer_liste(){
    return NULL;
}

int liste_vide(Liste L){
    return !L;
}

void visualiser_liste(Liste L){
    Liste p = L;
    for(p=L; !liste_vide(p); p=p->suiv){
        affiche(&p->val);
    }
}

Liste ajout_tete(ARC val, Liste L){
    Liste p;
    p=calloc(1, sizeof(*p));
    if (p==NULL) return NULL;
    p->val=val;
    p->suiv=L;
    return p;
}


Liste ajout_queue(ARC val, Liste L){
    if (liste_vide(L)){
        L=ajout_tete(val, L);
        return L;
    }
    Liste p = L;
    for(p=L; !liste_vide(p->suiv); p=p->suiv);
    Liste q=calloc(1, sizeof(*q));
    if (q==NULL) return NULL;
    q->val = val;
    q->suiv=p->suiv;
    p->suiv=q;
    return L;
}


ARC* rechercher_liste(Liste L, int dep, int arr){
    if(liste_vide(L)) return NULL;
    Liste q=NULL;
    for(q=L; !liste_vide(q); q=q->suiv){
        if(q->val.dep == dep && q->val.arr == arr) return &(q->val);
    }
    return NULL;
}

