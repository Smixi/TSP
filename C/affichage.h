#ifndef _AFFICHAGE
#define _AFFICHAGE
#include <stdio.h>
#include "structure.h"
#include <SDL_phelma.h>



void Clear(SDL_Surface* s1);
void tracer_segment(SDL_Surface* s1, double x1, double y1, double x2, double y2, double dimy, double pher, int decalage);
void tracer_graphe(SDL_Surface* s1, int nb_sommet, SOMMET* table, double dimy, double pher_max, int decalage);
void dim_fenetre(SOMMET* table, int nb_sommet, double* dimx, double* dimy);
void tracer_sol(SDL_Surface* s1, File solution, double dimx, double dimy, SOMMET* table, int decalage);
#endif
