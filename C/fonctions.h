#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "structure.h"
#include "liste.h"
#include "fonctions.h"
#include "file.h"
#include <SDL_phelma.h>
#include "affichage.h"
#include <time.h>

#define DECALAGE      50
#define EPSILON       0.00001       //Phèromones de départ sur chaque arc.
#define RHO           0.5           //Facteur d'évaporation des phèromones. (compris entre 0 et 1).
#define Q             1             //Quantité de phèromones que les fourmis dispersent au total par chemin.
#define alpha         1             //Importance accordée à la visibilité d'une ville lors du choix de la ville suivante (fonction choix_ville, variable à comparer avec beta).
#define beta          2             //Importance accordée au taux de phèromones présent sur l'arc entre deux villes lors du choix de la ville suivante.
#define COEF_FOURMIS  2             //Donne le nombre de fourmis à créer (nb_fourmis = COEF_FOURMIS * nb_sommet).

#define MAX_CYCLE     20            //Nombre de cycles à partir duquel on considère que l'algorithme a convergé vers une solution. (MAX_CYCLE < 5 augmente les chances de générer des solutions non optimales)
#define NB_ELITE      100           //Nombre de meilleurs chemins conservés en mémoire.
#define POIDS_ELITE   1             //Amplifie l'importance donnée aux meilleures solutions gardées en mémoires.
#define NB_RESET      50            //Donne le nombre de réitérations de l'algorithme.
#define STAGNE		  1				//Active l'optimisation liée à la variable stagne.

void visualiser_table(SOMMET* table, int nb_sommet);
FOURMIS* initialiser_fourmis(FOURMIS* tab_fourmis, int nb_fourmi, int nb_sommet);
double actualiser_pher(double longueur, File Plus_court_chemin);
void evaporer_pher(SOMMET* table, int nb_sommet);
unsigned int* initialiser_tabuk(unsigned int* tabuk, int nb_sommet, FOURMIS* tab_fourmis, int k);
int appartient_tab(unsigned int* tabu, int taille, int sommet);
SOMMET* creation_graphe(char* fichier, int* nb_sommet, int* nb_arc);
ARC* choix_ville(unsigned int* tabuk, Liste voisins);
File* actualiser_tab_elite(double* tab_longueur_elite, File* tab_elite,  File chemin_elite, double longueur_elite, int* stagne);
void reset_pher(SOMMET* table, int nb_sommet);
double TSP(char* fichier, int affichage, double* temps, File* chemin_opti);
#endif // FONCTIONS_H_INCLUDED
