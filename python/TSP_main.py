
from argparse import ArgumentParser
from os.path import isfile
import logging
from sys import exit
from re import search, finditer
from TSP_solver import Solver
from TSP_viewer import Viewer

def get_graph_definition(graph_file):
    """ Import distance map and node list from formatted file """

    if(not isfile(graph_file)):
        logging.error(f'File {graph_file} does not exist!')
        exit()

    with open(graph_file, 'r', encoding='utf-8') as f:
        file_dump = f.read()

    # Get number of nodes
    match = search(r'(\d+)\s+\d+\s*\n', file_dump)
    if(match):
        nb_node = int(match.group(1))
        logging.debug(f'Found nb_node = {nb_node} from line "{match.group(0).strip()}"')
    else:
        logging.error('Could not find number of nodes in the graph!')
        exit()

    # Parse nodes
    node_map = [[0, 0] for _ in range(nb_node)]
    for match in finditer(r'(\d+)\s+([\.\d]+)\s+([\.\d]+)\s+\w+\n', file_dump):
        node, x_pos, y_pos = int(match.group(1)), float(match.group(2)), float(match.group(3))
        logging.debug(f'Found node number {node} (X={x_pos}, Y={y_pos})')
        node_map[node] = [x_pos, y_pos]

    # Parse segments
    distance_map = [[0 for _ in range(nb_node)] for _ in range(nb_node)]
    for match in finditer(r'(\d+)\s+(\d+)\s+([\.\d]+)\s*\n', file_dump):
        start_node, end_node, distance = int(match.group(1)), int(match.group(2)), float(match.group(3))
        distance_map[start_node][end_node] = distance
        distance_map[end_node][start_node] = distance

    return node_map, distance_map, nb_node

if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument('-g', '--gui', action='store_true', default=False, help='Display GUI')
    parser.add_argument('-v', '--verbosity', type=int, default=logging.INFO, help='Verbosity level')
    parser.add_argument('-f', '--file', default='graphs/kroA100.txt', type=str, help='Graph file')

    args = parser.parse_args()

    logging.basicConfig(level=args.verbosity)

    node_map, distance_map, nb_node = get_graph_definition(args.file)

    solver = Solver(distance_map, nb_node)
    if(args.gui):
        viewer = Viewer(node_map, nb_node)

    for i in range(1000):
        solver.run()
        print(solver.elite_ant_colony[0].path_length)

        if(args.gui):
            viewer.update(solver.graph.pheromon_map)