import numpy as np
from random import random
from math import inf
from copy import deepcopy

class Graph:
    """ Holds the matrix of vertices with weight applied """

    def __init__(self, distance_map, nb_node, alpha=1, beta=2, rho=0.5, epsilon=0.00001):
        # TODO : Check inputs
        self.nb_node = nb_node
        self.alpha = alpha
        self.beta = beta
        self.rho = rho
        self.epsilon = epsilon
        self.distance_map = distance_map
        self.reset_pheromon_map()
        self.compute_inverted_map()
        self.compute_weighted_map()

    def reset_pheromon_map(self):
        """ Reset pheromon weigths """
        self.pheromon_map = np.array([[self.epsilon for _ in range(self.nb_node)] for _ in range(self.nb_node)])

    def compute_inverted_map(self):
        """ Inverse each coefficient of distance map and raise to power beta.
            Distances equal to 0 are considered unavailable paths."""
        self.inverted_map = [[1/self.distance_map[node1][node2] if(self.distance_map[node1][node2]) else 0 for node2 in range(self.nb_node)] for node1 in range(self.nb_node)]
        self.inverted_map = np.array(self.inverted_map)**self.beta

    def add_pheromon_weights(self, pheromon_weights):
        """ Update pheromon map by adding incoming weights """
        # TODO : Check input
        self.pheromon_map = self.pheromon_map + np.array(pheromon_weights)

    def compute_weighted_map(self):
        """ Get weight for each segment """
        self.weighted_map = self.inverted_map * self.pheromon_map**self.alpha

    def evaporate_pheromons(self):
        """ Reduce quantity of pheromons across the whole graph """
        self.pheromon_map = self.pheromon_map * self.rho

    def get_next_node(self, start_node, available_nodes):
        """ Randomize next node on path using weighted map available node list """
        # TODO : Check inputs
        # Compute sum of weights and roll a number in this range
        weight_sum = np.dot(self.weighted_map[start_node], np.array(available_nodes))
        roll = random()*weight_sum
        # Special exit case for paths impossible to complete
        if(weight_sum <= 0):
            return -1
        # Iterate candidate nodes until segment weight is higher than rolled weight
        for candidate_node in range(self.nb_node):
            if(available_nodes[candidate_node] and self.weighted_map[start_node][candidate_node] and self.weighted_map[start_node][candidate_node] >= roll):
                return candidate_node
            elif(available_nodes[candidate_node] and self.weighted_map[start_node][candidate_node]):
                roll -= self.weighted_map[start_node][candidate_node]
        # Should not reach here
        return -1

class Ant:
    """ Will find a path to connect all nodes of a graph """

    def __init__(self, start_node, nb_node):
        # TODO : Check inputs
        self.start_node = start_node
        self.current_node = start_node
        self.nb_node = nb_node
        self.reset_path()

    def reset_path(self):
        """ Rest path contents and available nodes """
        self.current_node = self.start_node
        self.available_nodes = [1 if(node != self.start_node) else 0 for node in range(self.nb_node)]
        self.path = [self.start_node]
        self.path_length = 0
        self.path_map = np.array([[0 for _ in range(self.nb_node)] for _ in range(self.nb_node)])

    def walk_to_node(self, next_node, walk_distance):
        """ Set current node and update path contents """
        # TODO : Check inputs
        self.path.append(next_node)
        self.available_nodes[next_node] = 0
        self.path_length += walk_distance
        self.path_map[self.current_node][next_node] = 1
        self.path_map[next_node][self.current_node] = 1
        self.current_node = next_node

class Solver:
    """ Solver for TSP """

    def __init__(self, distance_map, nb_node, **kwargs):
        # TODO : Check inputs
        self.nb_node = nb_node
        self.graph = Graph(distance_map, nb_node, **kwargs)
        self.ant_colony = [Ant(node%nb_node, nb_node) for node in range(2*nb_node)]
        self.best_path = []
        self.path_length = 0
        self.nb_elite_ants = nb_node
        self.elite_ant_colony = []
        self.stagnation_count = 0

    def walk_ants(self):
        """ Reset ants and make them walk the graph again.
            Sort ant colony by shortest path"""
        for ant in self.ant_colony:
            ant.reset_path()
            # Iterate for number of segments in the path (minus last segment)
            for _ in range(self.nb_node-1):
                next_node = self.graph.get_next_node(ant.current_node, ant.available_nodes)
                # Invalid path was found
                if(next_node == -1):
                    ant.reset_path()
                    ant.path_length = inf
                    break
                walk_distance = self.graph.distance_map[ant.current_node][next_node]
                ant.walk_to_node(next_node, walk_distance)
            # Add last segment
            walk_distance = self.graph.distance_map[ant.current_node][ant.start_node]
            if(walk_distance):
                ant.walk_to_node(ant.start_node, walk_distance)
            else:
                ant.reset_path()
                ant.path_length = inf

    def update_elite_ant_colony(self):
        """ Update elite ant colony with ants from current basic colony """
        best_path_reference = [ant.path_length for ant in self.elite_ant_colony]
        merged_colony = self.ant_colony + self.elite_ant_colony
        # Sort ants by shortest path
        sorted_colony = sorted(merged_colony, key=lambda ant: ant.path_length)
        self.elite_ant_colony = []
        for ant_number in range(self.nb_elite_ants):
            self.elite_ant_colony.append(deepcopy(sorted_colony[ant_number]))
        best_path_new = [ant.path_length for ant in self.elite_ant_colony]
        # Check is progess was made
        if(best_path_reference != best_path_new):
            self.stagnation_count = 0
        else:
            self.stagnation_count += 1

    def update_graph_pheromons(self):
        """ Compute new pheromons weights for the graph """
        self.graph.evaporate_pheromons()
        pheromon_weights = np.array([[0 for _ in range(self.nb_node)] for _ in range(self.nb_node)])
        # Each ant puts pheromons based on its path length
        for ant in self.ant_colony:
            if(ant.path_length < inf):
                pheromon_weights = pheromon_weights + ant.path_map/ant.path_length
        # Same principle for elite ants, with stronger weight
        for ant in self.elite_ant_colony:
            if(0 < ant.path_length < inf):
                pheromon_weights = pheromon_weights + ant.path_map/ant.path_length
        self.graph.add_pheromon_weights(pheromon_weights)

    def run(self):
        """ Iterate ant run to find best path """
        if(self.stagnation_count >= 20):
            self.graph.reset_pheromon_map()
            self.stagnation_count = 0
        self.graph.compute_weighted_map()
        self.walk_ants()
        self.update_elite_ant_colony()
        self.update_graph_pheromons()
