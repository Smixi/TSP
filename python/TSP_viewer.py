import matplotlib.pyplot as plt
from numpy import amax

class Viewer:

    def __init__(self, node_map, nb_node):
        self.fig, self.ax = plt.subplots()
        self.nb_node = nb_node
        self.plot = [[None for _ in range(nb_node)] for _ in range(nb_node)]
        for node_start in range(nb_node):
            for node_end in range(node_start+1, nb_node):
                self.plot[node_start][node_end] = self.ax.plot([node_map[node_start][0], node_map[node_end][0]], [node_map[node_start][1], node_map[node_end][1]], 'r', alpha=0.25)[0]
        self.fig.show()

    def update(self, pheromon_map):
        """ Get graph weights and plot most relevant ones """
        # Get maximum amount of pheromon of all the segments
        pheromon_max = amax(pheromon_map)
        # Color each segment relative to the current maximum amount of pheromon on the graph
        for node_start in range(self.nb_node):
            for node_end in range(node_start+1, self.nb_node):
                self.plot[node_start][node_end].set_visible(True)
                self.plot[node_start][node_end].set_alpha(pheromon_map[node_start][node_end]/pheromon_max)
                self.plot[node_start][node_end].set_color('green')

        self.ax.relim()
        self.ax.autoscale_view()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
